import XCTest

@testable import Cats

class EndpointsSpec: XCTestCase {

    func testPeopleEndpointIsNotNil() {
        XCTAssertNotNil(Endpoints.people)
    }

   func testPeopleEndpointIsCorrectURL() {
      let urlString = Endpoints.people?.absoluteString
      XCTAssertEqual(urlString, "http://agl-developer-test.azurewebsites.net/people.json")
   }
}
