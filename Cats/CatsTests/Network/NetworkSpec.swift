import XCTest
import OHHTTPStubs

@testable import Cats

class NetworkSpec: XCTestCase {

   var network: Network!
   var fetchExpectation: XCTestExpectation!

   override func setUp() {
      super.setUp()
      network = Network()
      fetchExpectation = XCTestExpectation(description: "fetch")
   }

   override func tearDown() {
      super.tearDown()
      OHHTTPStubs.removeAllStubs()
   }

   func testSuccessfullPeopleFetch() {

      stub(condition: isAbsoluteURLString(Endpoints.people!.absoluteString)) { _ in
         return OHHTTPStubsResponse(data: Person.testInstancePeopleJSON,
                                    statusCode: 200,
                                    headers: nil)
      }

      network.fetchPeople { result in
         switch result {
         case .success(let people): XCTAssertNotNil(people)
         case .failure: XCTFail()
         }
         self.fetchExpectation.fulfill()
      }

      XCTWaiter().wait(for: [fetchExpectation], timeout: 1)
   }

   func testFailedPeopleFetch() {

      stub(condition: isAbsoluteURLString(Endpoints.people!.absoluteString)) { _ in
         return OHHTTPStubsResponse(data: Person.testInstancePeopleJSON,
                                    statusCode: 400,
                                    headers: nil)
      }

      network.fetchPeople { result in
         switch result {
         case .success: XCTFail()
         case .failure: XCTAssertTrue(true)
         }
         self.fetchExpectation.fulfill()
      }

      XCTWaiter().wait(for: [fetchExpectation], timeout: 1)
   }
}
