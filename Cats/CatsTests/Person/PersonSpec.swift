import XCTest

@testable import Cats

class PersonSpec: XCTestCase {

   var personJSON: Data!
   var person: Person!

   override func tearDown() {
      super.tearDown()
      personJSON = nil
      person = nil
   }

   // MARK: - Decoding

   func testDecodingPersonFromValidJSON() {

      personJSON =
      """
      {
         "gender": "Female",
         "pets": [{"name": "Maru", "type": "Cat"}, {"name": "Toto", "type": "Dog"}, {"name": "Dory", "type": "Fish"}]
      }
      """.data(using: .utf8)!

      do {
         person = try JSONDecoder().decode(Person.self, from: personJSON)
      } catch {
         XCTFail("Decoding failed with valid data")
      }

      XCTAssertEqual(person.gender, .female)
      XCTAssertEqual(person.pets!, [Pet(name: "Maru", type: .cat), Pet(name: "Toto", type: .dog), Pet(name: "Dory", type: .fish)])
   }

   func testDecodingPersonFromValidJSONWithNoPets() {

      personJSON =
      """
      {
         "gender": "Female",
         "pets": null
      }
      """.data(using: .utf8)!

      do {
         person = try JSONDecoder().decode(Person.self, from: personJSON)
      } catch {
         XCTFail("Decoding failed with valid data")
      }

      XCTAssertNil(person.pets)
   }

   func testDecodingPersonFromInvalidJsonWithMissingProperty() {

      personJSON =
      """
      {
         "gender": "Female",
      }
      """.data(using: .utf8)!

      do {
         person = try JSONDecoder().decode(Person.self, from: personJSON)
      } catch DecodingError.keyNotFound(let key, _) {
         XCTAssertEqual(key.stringValue, "pets")
      } catch {
         XCTFail("Unexpected error thrown")
      }
   }

   func testDecodingPersonFromInvalidJsonWithUnknownType() {

      personJSON =
      """
      {
         "gender": "Klingon",
         "pets": null
      }
      """.data(using: .utf8)!

      do {
         person = try JSONDecoder().decode(Person.self, from: personJSON)
      } catch DecodingError.dataCorrupted(let context) {
         XCTAssertEqual(context.codingPath.first?.stringValue, "gender")
      } catch {
         XCTFail("Unexpected error thrown")
      }
   }

   // MARK: - CustomStringConvertible

   func testDescriptionOfGender() {
      XCTAssertEqual(Person.Gender.male.description, "Male")
      XCTAssertEqual(Person.Gender.female.description, "Female")
   }

   // MARK: - Filter by gender

   func testFilterByMale() {
      let males = People.testPeopleNoPets.males
      XCTAssertEqual(males.count, 2)
      males.forEach { XCTAssertEqual($0.gender, .male) }
   }

   func testFilterByFemale() {
      let females = People.testPeopleNoPets.females
      XCTAssertEqual(females.count, 3)
      females.forEach { XCTAssertEqual($0.gender, .female) }
   }

   // MARK: - Map to cats

   func testMapToCatsWithPeopleWithCats() {
      let cats = People.testPeopleWithPets.petsThatAreCats
      XCTAssertEqual(cats.count, 2)
      XCTAssertEqual(cats[0].name, "Felix")
      XCTAssertEqual(cats[1].name, "Milo")
   }

   func testMapToCatsWithPeopleWithoutPets() {
      let cats = People.testPeopleNoPets.petsThatAreCats
      XCTAssertEqual(cats.count, 0)
   }

   func testMapToCatsWithPeopleWithPetsThatAreNotCats() {
      let cats = People.testPeopleWithPetsThatAreNotCats.petsThatAreCats
      XCTAssertEqual(cats.count, 0)
   }
}

extension Array where Element == Person {
   
   static var testPeopleNoPets: People {
      return [Person(gender: .female, pets: nil),
              Person(gender: .male, pets: nil),
              Person(gender: .male, pets: nil),
              Person(gender: .female, pets: nil),
              Person(gender: .female, pets: nil)]
   }

   static var testPeopleWithPets: People {
      return [Person(gender: .female, pets: [Pet(name: "Felix", type: .cat), Pet(name: "Jaws", type: .fish)]),
              Person(gender: .male, pets: [Pet(name: "Milo", type: .cat), Pet(name: "Otis", type: .dog)]),
              Person(gender: .male, pets: nil),
              Person(gender: .female, pets: [Pet(name: "Cujo", type: .dog)])]
   }

   static var testPeopleWithPetsThatAreNotCats: People {
      return [Person(gender: .female, pets: [Pet(name: "Felix", type: .dog), Pet(name: "Jaws", type: .fish)]),
              Person(gender: .male, pets: [Pet(name: "Milo", type: .dog), Pet(name: "Otis", type: .dog)]),
              Person(gender: .male, pets: nil),
              Person(gender: .female, pets: [Pet(name: "Cujo", type: .dog)])]
   }

   static var testPeopleWithLotsOfCats: People {
      return [Person(gender: .female, pets: [Pet(name: "Echo", type: .cat), Pet(name: "Bravo", type: .cat), Pet(name: "Alpha", type: .cat)]),
              Person(gender: .female, pets: [Pet(name: "Delta", type: .cat), Pet(name: "Charlie", type: .cat), Pet(name: "Foxtrot", type: .cat)]),
              Person(gender: .male, pets: [Pet(name: "Delta", type: .cat), Pet(name: "Charlie", type: .cat), Pet(name: "Foxtrot", type: .cat)]),
              Person(gender: .male, pets: [Pet(name: "Echo", type: .cat), Pet(name: "Bravo", type: .cat), Pet(name: "Alpha", type: .cat)])]
   }
}

extension Person {

   static var testInstancePeopleJSON: Data {
      return """
      [{
         "gender": "Female",
         "pets": [{"name": "Maru", "type": "Cat"}, {"name": "Toto", "type": "Dog"}, {"name": "Dory", "type": "Fish"}]
      }]
      """.data(using: .utf8)!
   }
}
