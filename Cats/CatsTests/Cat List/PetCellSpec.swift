import XCTest

@testable import Cats

class PetCellSpec: XCTestCase {

   var vc: CatListViewController!
   var cell: PetCell!

   override func setUp() {
      super.setUp()
      vc = CatListViewController.testInstance
      cell = vc.tableView(vc.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as! PetCell
   }

   func testConfigurationWithViewModel() {
      XCTAssertEqual(cell.textLabel?.text, "Scar")
   }

   func testPreparingForReuseLeaveTheCellInTheCorrectState() {
      cell.prepareForReuse()
      XCTAssertEqual(cell.textLabel?.text, nil)
   }
}

extension CatListViewController {
   static var testInstance: CatListViewController {
      let vc = CatListViewController.testInstanceWithoutVM
      vc.viewModel = FakeCatListViewModel(cellViewModels: [PetCellViewModel(pet: Pet.scar)])
      return vc
   }

   static var testInstanceWithoutVM: CatListViewController {
      return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CatListViewController") as! CatListViewController
   }
}
