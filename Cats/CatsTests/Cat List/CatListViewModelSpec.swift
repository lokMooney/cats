import XCTest

@testable import Cats

class CatListViewModelSpec: XCTestCase {

   var viewModel: CatListViewModel!
   var fakeFetcher: FakeFetcher!
   var fakeUpdateDelegate: FakeUpdateDelegate!
   var updateExpectation: XCTestExpectation!

   func configureForTest(with result: FetchResult<People>) {
      updateExpectation = XCTestExpectation(description: "update")
      fakeFetcher = FakeFetcher(result: result)
      fakeUpdateDelegate = FakeUpdateDelegate(expectation: updateExpectation)
      viewModel = CatListViewModel(fetcher: fakeFetcher, updateDelegate: fakeUpdateDelegate)
   }

   func testHeaderForSection() {

      configureForTest(with: FetchResult<People>.success(People.testPeopleWithPets))

      XCTWaiter().wait(for: [updateExpectation], timeout: 1)

      XCTAssertEqual(viewModel.header(forSection: 0), "Male Owner")
      XCTAssertEqual(viewModel.header(forSection: 1), "Female Owner")
   }

   func testSuccessfullFetchWithDataReturned() {

      configureForTest(with: FetchResult<People>.success(People.testPeopleWithPets))

      XCTWaiter().wait(for: [updateExpectation], timeout: 1)

      XCTAssertEqual(fakeUpdateDelegate.updateSuccess, true)
      XCTAssertEqual(viewModel.numberOfSections, 2)
      XCTAssertEqual(viewModel.numberOfRows(inSection: 0), 1)
      XCTAssertEqual(viewModel.numberOfRows(inSection: 1), 1)
      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 0, section: 0)), PetCellViewModel(pet: Pet(name: "Milo", type: .cat)))
      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 0, section: 1)), PetCellViewModel(pet: Pet(name: "Felix", type: .cat)))
   }

   func testOrderOfCells() {

      configureForTest(with: FetchResult<People>.success(People.testPeopleWithLotsOfCats))

      XCTWaiter().wait(for: [updateExpectation], timeout: 1)

      XCTAssertEqual(fakeUpdateDelegate.updateSuccess, true)
      XCTAssertEqual(viewModel.numberOfSections, 2)
      XCTAssertEqual(viewModel.numberOfRows(inSection: 0), 6)
      XCTAssertEqual(viewModel.numberOfRows(inSection: 1), 6)

      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 0, section: 0)), PetCellViewModel(pet: Pet(name: "Alpha", type: .cat)))
      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 1, section: 0)), PetCellViewModel(pet: Pet(name: "Bravo", type: .cat)))
      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 2, section: 0)), PetCellViewModel(pet: Pet(name: "Charlie", type: .cat)))
      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 3, section: 0)), PetCellViewModel(pet: Pet(name: "Delta", type: .cat)))
      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 4, section: 0)), PetCellViewModel(pet: Pet(name: "Echo", type: .cat)))
      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 5, section: 0)), PetCellViewModel(pet: Pet(name: "Foxtrot", type: .cat)))

      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 0, section: 1)), PetCellViewModel(pet: Pet(name: "Alpha", type: .cat)))
      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 1, section: 1)), PetCellViewModel(pet: Pet(name: "Bravo", type: .cat)))
      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 2, section: 1)), PetCellViewModel(pet: Pet(name: "Charlie", type: .cat)))
      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 3, section: 1)), PetCellViewModel(pet: Pet(name: "Delta", type: .cat)))
      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 4, section: 1)), PetCellViewModel(pet: Pet(name: "Echo", type: .cat)))
      XCTAssertEqual(viewModel.cellViewModelForRow(at: IndexPath(row: 5, section: 1)), PetCellViewModel(pet: Pet(name: "Foxtrot", type: .cat)))
   }

   func testSuccessfullFetchWithNoDataReturned() {

      configureForTest(with: FetchResult<People>.success(People.testPeopleNoPets))

      XCTWaiter().wait(for: [updateExpectation], timeout: 1)

      XCTAssertEqual(fakeUpdateDelegate.updateSuccess, true)
      XCTAssertEqual(viewModel.numberOfSections, 2)
      XCTAssertEqual(viewModel.numberOfRows(inSection: 0), 0)
      XCTAssertEqual(viewModel.numberOfRows(inSection: 1), 0)
   }

   func testFailedFetch() {

      configureForTest(with: FetchResult<People>.failure(TestError()))

      XCTWaiter().wait(for: [updateExpectation], timeout: 1)

      XCTAssertEqual(fakeUpdateDelegate.updateSuccess, false)
      XCTAssertEqual(viewModel.numberOfSections, 0)
      XCTAssertEqual(viewModel.numberOfRows(inSection: 0), 0)
   }
}

// MARK - Fakes

class FakeUpdateDelegate: CatListViewModelUpdateDelegate {

   var updateSuccess: Bool? = nil

   var expectation: XCTestExpectation

   init(expectation: XCTestExpectation) {
      self.expectation = expectation
   }

   func modelUpdated(successfully: Bool) {
      updateSuccess = successfully
      expectation.fulfill()
   }
}

class FakeFetcher: Fetcher {

   let result: FetchResult<People>

   init(result: FetchResult<People>) {
      self.result = result
   }

   func fetchPeople(completionHandler: @escaping PeopleFetchCompletion) {
      completionHandler(result)
   }
}

struct TestError: Error {}
