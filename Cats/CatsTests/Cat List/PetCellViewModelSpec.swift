import XCTest

@testable import Cats

class PetCellViewModelSpec: XCTestCase {

   func testViewModelIsCorrectlyInitialisedFromAPetModel() {
      let viewModel = PetCellViewModel(pet: Pet.simba)
      XCTAssertEqual(viewModel.name, "Simba")
   }

   func testViewModelComparableAndEquatable() {

      let nalaVM = PetCellViewModel(pet: Pet.nala)
      let scarVM = PetCellViewModel(pet: Pet.scar)
      let simbaVM = PetCellViewModel(pet: Pet.simba)

      XCTAssertEqual(nalaVM < scarVM, true)
      XCTAssertEqual(scarVM < simbaVM, true)
      XCTAssertEqual(simbaVM < nalaVM, false)
   }
}

extension Pet {
   static var nala: Pet { return Pet(name: "Nala", type: .cat) }
   static var scar: Pet { return Pet(name: "Scar", type: .cat) }
   static var simba: Pet { return Pet(name: "Simba", type: .cat) }
}
