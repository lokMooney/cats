import XCTest

@testable import Cats

class CatListViewControllerSpec: XCTestCase {

   var vc: CatListViewController!

   func setUpWithVM() {
      vc = CatListViewController.testInstance
      UIApplication.shared.keyWindow?.rootViewController = vc
   }

   func setUpWithoutVM() {
      vc = CatListViewController.testInstanceWithoutVM
      UIApplication.shared.keyWindow?.rootViewController = vc
   }

   func testTitleIsSetCorrectlyWhenViewIsLoaded() {
      setUpWithVM()
      XCTAssertEqual(vc.title, "Cats")
   }

   func testTableViewIsLoadedWhenModelIsUpdatedSuccessfully() {
      setUpWithVM()
      vc.modelUpdated(successfully: true)
      XCTAssertEqual(vc.numberOfSections(in: vc.tableView), 1)
      XCTAssertEqual(vc.tableView(vc.tableView, numberOfRowsInSection: 0), 1)
      XCTAssertEqual(vc.tableView(vc.tableView, titleForHeaderInSection: 0), "section header")
      XCTAssertEqual(vc.tableView(vc.tableView, cellForRowAt: IndexPath(row: 0, section: 0)).textLabel?.text, "Scar")
   }

   func testErrorStateIsShownWhenModelIsUpdatedUnsuccessfully() {
      setUpWithVM()
      XCTAssertEqual(vc.childViewControllers.isEmpty, true)
      vc.modelUpdated(successfully: false)
      XCTAssertEqual(vc.childViewControllers.isEmpty, false)
   }

   func testTableViewIsEmptyIfViewIsLoadedWithoutViewModel() {
      setUpWithoutVM()
      XCTAssertEqual(vc.numberOfSections(in: vc.tableView), 0)
      XCTAssertEqual(vc.tableView(vc.tableView, numberOfRowsInSection: 0), 0)
      XCTAssertEqual(vc.tableView(vc.tableView, titleForHeaderInSection: 0), nil)
   }
}
