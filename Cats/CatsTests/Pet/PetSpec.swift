import XCTest

@testable import Cats

class PetSpec: XCTestCase {

   var petJSON: Data!
   var pet: Pet!

   override func tearDown() {
      super.tearDown()
      petJSON = nil
      pet = nil
   }

   // MARK: - Decoding

   func testDecodingPetFromValidJSON() {

      petJSON =
      """
      {
         "name": "Tigger",
         "type": "Cat"
      }
      """.data(using: .utf8)!

      do {
         pet = try JSONDecoder().decode(Pet.self, from: petJSON)
      } catch {
         XCTFail("Decoding failed with valid data")
      }

      XCTAssertEqual(pet.name, "Tigger")
      XCTAssertEqual(pet.type, .cat)
   }

   func testDecodingPetFromInvalidJsonWithMissingProperty() {

      petJSON =
      """
      {
         "type": "Cat"
      }
      """.data(using: .utf8)!

      do {
         pet = try JSONDecoder().decode(Pet.self, from: petJSON)
      } catch DecodingError.keyNotFound(let key, _) {
         XCTAssertEqual(key.stringValue, "name")
      } catch {
         XCTFail("Unexpected error thrown")
      }
   }

   func testDecodingPetFromInvalidJsonWithUnknownType() {

      petJSON =
      """
      {
         "name": "Tigger",
         "type": "Elephant"
      }
      """.data(using: .utf8)!

      do {
         pet = try JSONDecoder().decode(Pet.self, from: petJSON)
      } catch DecodingError.dataCorrupted(let context) {
         XCTAssertEqual(context.codingPath.first?.stringValue, "type")
      } catch {
         XCTFail("Unexpected error thrown")
      }
   }

   // MARK: - CustomStringConvertible

   func testDescriptionOfSpecies() {
      XCTAssertEqual(Pet.Species.cat.description, "Cat")
      XCTAssertEqual(Pet.Species.dog.description, "Dog")
      XCTAssertEqual(Pet.Species.fish.description, "Fish")
   }
}
