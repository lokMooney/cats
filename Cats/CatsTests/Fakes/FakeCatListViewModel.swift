import Foundation

@testable import Cats

class FakeCatListViewModel: CatListViewModelType {

   init(cellViewModels: [PetCellViewModel]) {
      self.cellViewModels = cellViewModels
   }

   var cellViewModels: [PetCellViewModel]

   var numberOfSections: Int { return 1 }

   func numberOfRows(inSection section: Int) -> Int {
      return cellViewModels.count
   }

   func cellViewModelForRow(at indexPath: IndexPath) -> PetCellViewModel {
      return cellViewModels[indexPath.row]
   }

   func header(forSection section: Int) -> String? {
      return "section header"
   }
}
