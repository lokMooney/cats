import Foundation

// MARK: - Fetcher


/// Defines a fetcher of data
protocol Fetcher {

   /// A request to fetch People data (with pets)
   ///
   /// - Parameter completionHandler: Called on fetch completion, returns either People or an Error
   func fetchPeople(completionHandler: @escaping PeopleFetchCompletion)
}


/// Completion handler for fetching People (with pets)
typealias PeopleFetchCompletion = (FetchResult<People>) -> Void


/// Fetch result type
///
/// - success: The fetch was successfull. This wraps the resulting data.
/// - failure: The fetch failed. This wraps the occuring error.
enum FetchResult<T> {
   case success(T)
   case failure(Error)
}

enum FetcherError: Error {
   case fetchFailed
}

// MARK: - Network

// NOTE: I'd usually use AlamoFire for the network layer and a JSON parsing library (like SwiftyJSON), but I figured in this instance it wasn't worth the dependencies...and I've been keen to test out Codable 

/// A concrete Fetcher that uses URLSession to fetch data
final class Network: Fetcher {


   func fetchPeople(completionHandler:  @escaping PeopleFetchCompletion) {

      guard let url = Endpoints.people else {
         fatalError("Programmer error: Invalid endpoint for people fetch")
      }

      let fetchTask = URLSession.shared.dataTask(with: url) { data, response, error in

         // Catch task error
         if let fetchError = error {
            completionHandler(.failure(fetchError))
            return
         }

         // Parse response
         if let response = response as? HTTPURLResponse, let fetchData = data {

            switch response.statusCode {
            case 400...599: completionHandler(.failure(FetcherError.fetchFailed))
            default:
               do {
                  let people = try JSONDecoder().decode(People.self, from: fetchData)
                  completionHandler(.success(people))
               } catch {
                  completionHandler(.failure(error))
               }
            }
         } else {
            completionHandler(.failure(FetcherError.fetchFailed))
         }
      }

      fetchTask.resume()
   }
}
