import Foundation

/// The endpoints required for the application.
struct Endpoints {

   /// The endpoint for fetching a list of people and their pets.
   static let people = URL(string: "http://agl-developer-test.azurewebsites.net/people.json")
}
