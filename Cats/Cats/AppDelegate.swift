import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

   var window: UIWindow?

   func application(_ application: UIApplication,
                    didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

      // If the application is running unit tests we don't want to go down the normal path

      guard !UserDefaults.standard.bool(forKey: "isRunningTests") else {
         return true
      }


      // Configure the cat list

      guard let catList = (window?.rootViewController as? UINavigationController)?.topViewController as? CatListViewController else {
         fatalError("Programmer Error: initial viewcontroller is not correctly configured")
      }

      catList.viewModel = CatListViewModel(fetcher: Network(), updateDelegate: catList)

      return true
   }
}
