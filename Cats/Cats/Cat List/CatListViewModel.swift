import Foundation

/// Interface for the view-model to drive the cat list
protocol CatListViewModelType {
   var numberOfSections: Int { get }
   func numberOfRows(inSection section: Int) -> Int
   func cellViewModelForRow(at indexPath: IndexPath) -> PetCellViewModel
   func header(forSection section: Int) -> String?
}

/// Interface for the cat list update delegate
protocol CatListViewModelUpdateDelegate: AnyObject {
   func modelUpdated(successfully: Bool)
}

/// Defines the view-model fro driving the cat list
final class CatListViewModel: CatListViewModelType {

   private var catCellViewModels = [[PetCellViewModel]]()
   private weak var updateDelegate: CatListViewModelUpdateDelegate?

   init(fetcher: Fetcher, updateDelegate: CatListViewModelUpdateDelegate) {
      self.updateDelegate = updateDelegate
      fetcher.fetchPeople(completionHandler: handlePeopleFetch)
   }

   /// The number of sections in the cat list
   var numberOfSections: Int { return catCellViewModels.count }

   func numberOfRows(inSection section: Int) -> Int {
      guard section < catCellViewModels.count else { return 0 }
      return catCellViewModels[section].count
   }

   func cellViewModelForRow(at indexPath: IndexPath) -> PetCellViewModel {

      guard
         indexPath.section < catCellViewModels.count,
         indexPath.row < catCellViewModels[indexPath.section].count
      else { fatalError("Programmer Error: datasource and tableview is out of sync") }

      return catCellViewModels[indexPath.section][indexPath.row]
   }

   func header(forSection section: Int) -> String? {
      switch section {
      case 0: return "Male Owner"
      case 1: return "Female Owner"
      default: return nil
      }
   }
}

// MARK: - Private
private extension CatListViewModel {

   func handlePeopleFetch(result: FetchResult<People>) {
      switch result {
      case .success(let people): update(with: people)
      case .failure(let error): update(with: error)
      }
   }

   func update(with people: People) {

      let catsOwnedByMales = people.males.petsThatAreCats
         .map { PetCellViewModel(pet: $0) }
         .sorted(by: <)

      let catsOwnedByFemales = people.females.petsThatAreCats
         .map { PetCellViewModel(pet: $0) }
         .sorted(by: <)

      catCellViewModels = [catsOwnedByMales, catsOwnedByFemales]

      notifyDelegateOfUpdate(success: true)
   }

   func update(with error: Error) {
      notifyDelegateOfUpdate(success: false)
   }

   func notifyDelegateOfUpdate(success: Bool) {
      DispatchQueue.main.async { [unowned self] in self.updateDelegate?.modelUpdated(successfully: success) }
   }
}
