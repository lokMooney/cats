import UIKit

/// A tableview that displays a list of cats grouped by their owners gender, in alphabetical order.
final class CatListViewController: UITableViewController {

   var viewModel: CatListViewModelType?

   // MARK: - Lifecycle

   override func viewDidLoad() {
      super.viewDidLoad()
      title = "Cats"
   }

   // MARK: - UITableViewDataSource

   override func numberOfSections(in tableView: UITableView) -> Int {
      return viewModel?.numberOfSections ?? 0
   }

   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return viewModel?.numberOfRows(inSection: section) ?? 0
   }

   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

      guard let cellViewModel = viewModel?.cellViewModelForRow(at: indexPath) else {
         fatalError("Programmer Error: datasource and tableview is out of sync")
      }

      guard let cell = tableView.dequeueReusableCell(withIdentifier: "PetCell", for: indexPath) as? PetCell else {
         fatalError("Programmer Error: cell has not be registered correctly with tableview")
      }

      cell.viewModel = cellViewModel
      return cell
   }

   override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
      return viewModel?.header(forSection: section) ?? nil
   }
}

// MARK: - CatListViewModelUpdateDelegate

extension CatListViewController: CatListViewModelUpdateDelegate {

   func modelUpdated(successfully: Bool) {

      loadViewIfNeeded()

      if successfully {
         tableView.reloadData()
      } else {
         showErrorState()
      }
   }
}

// MARK: - Show Error state

private extension CatListViewController {
   func showErrorState() {
      let errorView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ErrorView")
      errorView.view.frame = view.bounds
      addChildViewController(errorView)
      view.addSubview(errorView.view)
      view.bringSubview(toFront: errorView.view)
      errorView.didMove(toParentViewController: self)
   }
}
