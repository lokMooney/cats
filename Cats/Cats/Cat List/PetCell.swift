import UIKit

/// A tableview cell for displaying pets
final class PetCell: UITableViewCell {

   var viewModel: PetCellViewModel? {
      didSet { textLabel?.text = viewModel?.name }
   }

   override func prepareForReuse() {
      super.prepareForReuse()
      textLabel?.text = nil
   }
}
