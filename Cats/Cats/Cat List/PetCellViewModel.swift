import Foundation

/// Defines the view-model for displaying a pet in a cell
struct PetCellViewModel: Equatable {

   let name: String

   init(pet: Pet) {
      self.name = pet.name
   }
}

// MARK: - Comparable

extension PetCellViewModel: Comparable {
   static func < (lhs: PetCellViewModel, rhs: PetCellViewModel) -> Bool {
      return lhs.name < rhs.name
   }
}
