import Foundation

/// Defines a pet
struct Pet: Decodable, Equatable {

   /// Defines the type of pet
   ///
   /// - cat: 🐈
   /// - dog: 🐕
   /// - fish: 🐠
   /// - other: 🤷‍♂️
   enum Species: String, Decodable, Equatable {
      case cat = "Cat"
      case dog = "Dog"
      case fish = "Fish"
   }

   let name: String
   let type: Species
}

// MARK: - CustomStringConvertible

extension Pet.Species: CustomStringConvertible {
   var description: String {
      return self.rawValue
   }
}
