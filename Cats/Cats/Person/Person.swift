import Foundation

/// Defines a person with a gender and pets
struct Person: Decodable {

   /// Defines a persons binary genders (sorry non-binary folks)
   ///
   /// - male: ♂
   /// - female: ♀
   enum Gender: String, Decodable {
      case male = "Male"
      case female = "Female"
   }

   // NOTE: Only the properties required to meet the current requirements are being parsed.

   let gender: Gender
   let pets: [Pet]?
}

// MARK: - CustomStringConvertible

extension Person.Gender: CustomStringConvertible {
   var description: String {
      return self.rawValue
   }
}

// MARK: - People

typealias People = [Person]

// MARK: - Filter by gender

extension Array where Element == Person {

   /// Only the males
   var males: People {
      return filter { $0.gender == .male }
   }

   // Only the females
   var females: People {
      return filter { $0.gender == .female }
   }
}

// MARK: - Map to cats

extension Array where Element == Person {

   /// All of the peoples pets that are cats
   var petsThatAreCats: [Pet] {
      let allPets = compactMap { $0.pets }.flatMap { $0 }
      return allPets.filter { $0.type == .cat }
   }
}
