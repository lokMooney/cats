# Cats

Cats is an iOS application that displays a list of cats in alphabetical order, under a heading of the gender of their owner.

## Requirements

- iOS 11.3
- Xcode 9.3+
- Swift 4.1+

## Devices

iPhone

## Installation

Open project in Xcode, select a suitable device simulator and Run (⌘R)

## Unit tests

Open project in Xcode, select a suitable target device and Test (⌘U)
